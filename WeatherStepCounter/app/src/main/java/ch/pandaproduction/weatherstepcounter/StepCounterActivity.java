package ch.pandaproduction.weatherstepcounter;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StepCounterActivity extends DrawerActivity implements SensorEventListener {

    private SensorManager sensorManager;

    private TextView tv_steps;

    private boolean running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_counter);
        tv_steps = (TextView) findViewById(R.id.tv_steps);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        onPause();

        final Button startBtn = (Button) findViewById(R.id.start_btn);
        final Button stopBtn = (Button) findViewById(R.id.stop_btn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("a", "button start pressed");
                stopBtn.setEnabled(true);
                startBtn.setEnabled(false);
                onResume();
            }
        });

        stopBtn.setEnabled(false);

        stopBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("a", "button stop pressed");
                stopBtn.setEnabled(false);
                startBtn.setEnabled(true);
                onPause();
            }
        });

        this.createDrawer();
    }

    @Override
    protected void onResume() {

        super.onResume();
        running = true;
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(this, "Sensor not found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        running = false;
        //sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (running) {
            tv_steps.setText(String.valueOf(event.values[0]));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
