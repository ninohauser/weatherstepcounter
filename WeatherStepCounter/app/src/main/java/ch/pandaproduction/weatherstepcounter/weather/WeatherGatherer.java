package ch.pandaproduction.weatherstepcounter.weather;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ch.pandaproduction.weatherstepcounter.weather.json.WeatherEntity;
import ch.pandaproduction.weatherstepcounter.weather.location_service.Position;

import static java.lang.String.format;

public class WeatherGatherer extends AsyncTask {

    public WeatherGatherer() {
    }

    @Override
    protected Object doInBackground(Object... objects) {
        return getWeatherData((Position) objects[0]);
    }

    private WeatherEntity getWeatherData(Position position) {
        String apiUrl = format("http://api.apixu.com/v1/current.json?key=34d8bf2feea24de7ba8115951170609&q=%s,%s", position.getLatitude(), position.getLongitude());
        try {
            URL url = new URL(apiUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return parseResponseToJson(response.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private WeatherEntity parseResponseToJson(String response) {
        try {
            Gson gson = new GsonBuilder().create();
            return gson.fromJson(response, WeatherEntity.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not parse response to Weather entity; Response: '" + response + "'");
        }
    }
}
