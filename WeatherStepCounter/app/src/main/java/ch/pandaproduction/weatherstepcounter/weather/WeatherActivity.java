package ch.pandaproduction.weatherstepcounter.weather;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import ch.pandaproduction.weatherstepcounter.DrawerActivity;
import ch.pandaproduction.weatherstepcounter.R;
import ch.pandaproduction.weatherstepcounter.weather.json.WeatherEntity;
import ch.pandaproduction.weatherstepcounter.weather.location_service.GPSTracker;
import ch.pandaproduction.weatherstepcounter.weather.location_service.Position;

public class WeatherActivity extends DrawerActivity {

    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather);
        gps = new GPSTracker(this);
        Position position = getPosition();
        try {
            WeatherEntity weatherEntity = (WeatherEntity) new WeatherGatherer()
                    .execute(position)
                    .get();
            TextView humidity = (TextView) findViewById(R.id.humidity);
            humidity.setText(weatherEntity.getCurrent().getHumidity().toString() + "%");

            TextView temparature = (TextView) findViewById(R.id.temparature);
            temparature.setText(weatherEntity.getCurrent().getTemp_c().toString() + " C°");

            TextView wind = (TextView) findViewById(R.id.wind);
            wind.setText(weatherEntity.getCurrent().getWind_kph().toString() + " km/h");

            TextView location = (TextView) findViewById(R.id.location);
            location.setText(weatherEntity.getLocation().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(this, "Could not get Weather. It has been interrupted. Please try again later.", Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {
            e.printStackTrace();
            Toast.makeText(this, "Could not get Weather. There was an unknown error. Please try again later.", Toast.LENGTH_SHORT).show();
        }
        this.createDrawer();
    }

    private Position getPosition() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (gps.canGetLocation()) {
                return new Position(gps.getLatitude(), gps.getLongitude());
            } else {
                gps.showSettingsAlert();
                return new Position(0, 0);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 100);
            getPosition();
            //should never come to this?!
            return null;
        }
    }
}
